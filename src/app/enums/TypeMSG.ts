export enum TypeMSG {//Tipos de mensagens
    "DANGER"                                    = "MSG_DANGER",
    "SUCCESS"                                   = "MSG_SUCCESS",
    "INFO"                                      = "MSG_INFO",
    "ALERT"                                     = "MSG_ALERT"
}