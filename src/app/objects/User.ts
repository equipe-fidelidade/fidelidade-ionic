import {Profile} from "./Profile";
export class User{
    id?: number;

    name?: string;
    
    cpf?: string;

    email?: string;

    login?: string;
    
    password?: string;
    
    photoProfile?: string;

    isAdmin?: boolean;

    isActive?: boolean;

    profiles?: Profile[];
}