import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, Platform } from '@ionic/angular';
import { AppComponent } from 'src/app/app.component';
import { User } from 'src/app/objects/user';
import { LoginService } from 'src/app/services/login.service';
import { UtilService } from 'src/app/services/util.service';
import { LANG_PT_TRANS } from 'src/app/utils/translate/lang-pt';
import { TranslateService } from 'src/app/utils/translate/translate.service';
import { async } from '@angular/core/testing';
import { ObjectMSG } from 'src/app/enums/ObjectMSG';
import { SecretToken, Routers } from 'src/app/utils/values';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private user: User = {login: "mumps", password:"123456"};
  constructor(
    private app: AppComponent,
    private platform: Platform,
    private loginService: LoginService,
    private navController: NavController,
    private utilService: UtilService,
    private translate: TranslateService,
    public menuController: MenuController
    ) { }
    
    async ngOnInit() {
      this.menuController.enable(false);
      await this.platform.ready();
      this.app.userLogged=  undefined;
      this.app.links = [];
  }
  
  async logout(){
    try {
      await this.utilService.showLoading();
      console.log("Chegou no logout");
      let result = await this.loginService.logout();
      console.log(result);
      
      this.utilService.toask(this.translate.getValue(LANG_PT_TRANS.LOGOUT_SUCCESSFULLY));
    } catch (error) {
       console.log(error);
    }finally{
      await this.utilService.dismissLoading();
    }
  }
  
  async login(){
    await this.utilService.showLoading();
    this.loginService.login(this.user)
    .subscribe(async data => {
      await this.app.setUser(data[ObjectMSG.OBJ]);
      await this.utilService.setStorage(SecretToken.TOKEN, data[ObjectMSG.ACCESS_TOKEN]);
      await this.menuController.enable(true);
      await this.navController.navigateRoot(Routers.HOME);
    }, async error => {
      await this.utilService.showMesseges(error);
    }, async () => {
      await this.utilService.dismissLoading();
    });
  }

}
