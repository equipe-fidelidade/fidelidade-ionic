import { TRANSLATION_PROVIDERS } from './translations';
import { TranslatePipe } from './translate.pipe';
import { TranslateService } from './translate.service';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [],
  declarations: [TranslatePipe],
  exports: [TranslatePipe],
  providers: [
    TranslateService, TRANSLATION_PROVIDERS
  ]
})
export class TranslateModule { }