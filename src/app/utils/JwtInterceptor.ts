import { HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { finalize, mergeMap, tap } from 'rxjs/operators';
import { UtilService } from '../services/util.service';
import { SecretToken } from './values';
 
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private utilService: UtilService) {}
    
    intercept(req: HttpRequest<any>, next: HttpHandler) {
    let ok: string;
    let promisseToken = this.utilService.getStorage(SecretToken.TOKEN);
        return from(promisseToken).pipe(
            mergeMap(token => {
                let reqClone = this.addToken(req, token);
                return next.handle(reqClone)
                .pipe(
                    tap(
                        event => ok = event instanceof HttpResponse ? 'succeeded' : '',
                        error => ok = 'failed'
                    ),
                    finalize(() => {
                    })
                );
            })
        )
    }
    
    // return Observable.fromPromisse(promisseToken).
    // let cloneRequest = this.addToken(req, );
//     let authReq = req.clone({
//         headers: req.headers.set('Authorization', token)
//     });
 
//     return next.handle(authReq)
//       .pipe(
//         tap(
//           event => ok = event instanceof HttpResponse ? 'succeeded' : '',
//           error => ok = 'failed'
//         ),
//         // Log when response observable either completes or errors
//         finalize(() => {
//         })
//       );
//   }

  private  addToken(request: HttpRequest<any>, token: any) {    
      if(token){
          let tokenName = SecretToken.TOKEN;
          request = request.clone({
            setHeaders:{
                Accept: "application/json",
                "Content-type": "application/json",
                tokenName: token
            }
          });
      }
      return request;
  }
}