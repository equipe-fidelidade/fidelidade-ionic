import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, UrlTree } from '@angular/router';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  constructor(
    private navController: NavController,
    private loginService: LoginService
  ){}

  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;

  canActivate(): Observable<boolean|UrlTree>|Promise<boolean|UrlTree>|boolean|UrlTree {

    
    // console.log(this.loginService.getAuth().currentUser);
    // if(!this.loginService.getAuth().currentUser){
      
    //   console.log("Tem usuário logado");
    //   this.navController.navigateForward(routers.LOGIN);
    // }
    return true;
  }
}
