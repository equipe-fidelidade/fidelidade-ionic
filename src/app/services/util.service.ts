import { DOCUMENT } from "@angular/common";
import { HttpErrorResponse } from "@angular/common/http";
import { Inject, Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import * as Color from "color";
import * as cryptojs from "crypto-js";
import { TranslateService } from '../utils/translate/translate.service';
import { SecretToken } from '../utils/values';
import { ObjectMSG } from '../enums/ObjectMSG';
import { TypeMSG } from '../enums/TypeMSG';
import Validation from '../utils/Validation';
import { Storage } from '@ionic/storage';

@Injectable({
    providedIn: "root"
})
export class UtilService {

    private loadingNow: HTMLIonLoadingElement;

    constructor(
        private translate: TranslateService,
        private alertController: AlertController,
        private toastController: ToastController,
        private loadingController: LoadingController,
        private modalController: ModalController,
        @Inject(DOCUMENT) private document: Document,
        private storage: Storage
    ) {
    }

    public setColorTheme(primary: string, secundary, tertiary) {
        let colorTheme = this.cssGenerator(primary, secundary, tertiary);
        this.setGlobalCss(colorTheme);
    }

    private setGlobalCss(css: string) {
        this.document.documentElement.style.cssText = css;
        console.log(this.document.documentElement.style.cssText);
    }

    private getContrast(color, ratio = 0.8){
        let colorC = Color(color);
        console.log(`IsDark: ${colorC.isDark()}`)
        console.log(`IsDark: ${colorC.whiten(1)}`)
        return colorC.isDark() ? colorC.mix(Color("white"), 0.9): colorC.blacken(ratio);
    }

    private cssGenerator(primaryColor: string, secundaryColor: string, tertiaryColor: string){
        let ratioTint = 0.1;
        let ratioShade = 0.1;
        let ratioSecondary = 0.4;
        let ratioTertiary = 0.3;
        let primary = Color(primaryColor);
        let secondary = secundaryColor ? Color(secundaryColor) :primary.lighten(ratioSecondary);
        // secondary = secondary.saturate(0.8);
        // --ion-item-background: rgba(255, 255, 255, 1);
        let tertiary = tertiaryColor ? Color(tertiaryColor) : primary.darken(ratioTertiary);
        return `
            --ion-color-primary: ${primary};
            --ion-color-primary-contrast: ${this.getContrast(primary)};
            --ion-color-primary-shade: ${primary.darken(ratioShade)};
            --ion-color-primary-tint: ${primary.lighten(ratioTint)};
            --ion-color-secondary: ${secondary};
            --ion-color-secondary-contrast: ${this.getContrast(secondary)};
            --ion-color-secondary-shade: ${secondary.darken(ratioShade)};
            --ion-color-secondary-tint: ${secondary.lighten(ratioTint)};
            --ion-color-tertiary: ${tertiary};
            --ion-color-tertiary-contrast: ${this.getContrast(tertiary)};
            --ion-color-tertiary-shade: ${tertiary.darken(ratioShade)};
            --ion-color-tertiary-tint: ${tertiary.lighten(ratioTint)};
            --ion-item-background: #fff;
            --ion-background-color: linear-gradient(142deg, ${primary.desaturate(0.8).mix(Color("white"), 0.7)} 10%,  ${primary.desaturate(0.8).fade(0.4)} 90%);
        `;
    }

    public hMacSha512(text: string): string {
        try {
            let hash = cryptojs.HmacSHA512(text, SecretToken.SECRET_PUBLIC);
            return hash.toString();
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + text);
            return "";
        }
    }

    public encrypt(text: string): string {
        try {
            let ciphertext = cryptojs.AES.encrypt(JSON.stringify(text), SecretToken.SECRET_PUBLIC).toString();
            return ciphertext;
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + text);
            return "";
        }
    }

    public decrypt(ciphertext: any): string {
        try {
            let bytes = cryptojs.AES.decrypt(ciphertext.toString(), SecretToken.SECRET_PUBLIC);
            var decryptedData = JSON.parse(bytes.toString(cryptojs.enc.Utf8));
            return decryptedData;
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + ciphertext);
            return "";
        }
    }

    public getOnlyNumbers(value) {
        if (value) {
            try {
                return value.replace(/[^0-9]+/g, "");
            } catch (e) { console.log(e); }
        }
        return null;
    };

    public getMsgStatusError(status) {
        let msg: string = this.translate.getValue("statusDefault");
        switch (status) {
            case 0: msg = this.translate.getValue("status0"); break;
            case 302: msg = this.translate.getValue("status302"); break;
            case 304: msg = this.translate.getValue("status304"); break;
            case 400: msg = this.translate.getValue("status400"); break;
            case 401: msg = this.translate.getValue("status401"); break;
            case 403: msg = this.translate.getValue("status403"); break;
            case 404: msg = this.translate.getValue("status404"); break;
            case 405: msg = this.translate.getValue("status405"); break;
            case 410: msg = this.translate.getValue("status410"); break;
            case 500: msg = this.translate.getValue("status500"); break;
            case 502: msg = this.translate.getValue("status302"); break;
            case 503: msg = this.translate.getValue("status302"); break;
            default: break;
        }
        return msg;

    };

    public getItemList = (lista, value, campo) => {
        if (!campo) { campo = "id" };
        var item = null;
        if (value && lista) {
            for (var index = 0; index < lista.length; index++) {
                if (lista[index][campo] == value) {
                    item = lista[index];
                    break;
                }
            }
        }
        return item;
    };

    public getCountDuplicateList = (lista, value, campo) => {
        let count = 0;
        if (!campo) { campo = "id" };
        var item = null;
        if (value && lista) {
            for (var index = 0; index < lista.length; index++) {
                if (lista[index][campo] == value) {
                    count++;
                }
            }
        }
        return count;
    };

    public removeItemList = (lista, value, campo) => {
        if (!campo) { campo = "id" };
        var item = null;
        if (value && lista) {
            for (var index = 0; index < lista.length; index++) {
                if (lista[index][campo] == value) {
                    lista.splice(index, 1);
                    break;
                }
            }
        }
    };

    public filterItemList = (lista, value: string, campo: string) => {
        if (!campo) { campo = "id" };
        if (!value) return lista;
        return lista.filter(item => {
            return item[campo].toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) != -1;
        });
    };

    public changeItemList(oldList: Array<any>, newList: Array<any>, item: any) {
        if (oldList && newList) {
            let index = oldList.indexOf(item);
            if (index > -1) {
                oldList.splice(index, 1);
            }
            newList.push(item);
        }
    }

    public getValueObjectField(obj: object, field: string) {
        if (field.includes(".")) {
            let fields = field.split(".");
            fields.forEach((fieldSplit) => {
                obj = obj[fieldSplit];
            });
            return obj;
        }
        return obj[field] || "";
    }

    public createValidator(func: any, msg: string): ValidatorFn {
        return (control: AbstractControl): { [key: string]: string } | null => {
            if (!func(control.value)) {
                msg = this.translate.getValue(msg);
                return { 'msgErro': msg };
            }
            return null;
        };
    }

    public getFilterPagination(filter) {
        let newFilter: string = "?";
        let page = (filter.page) ? "page=" + filter.page : "&page=1";
        let limit = (filter.limit) ? "&limit=" + filter.limit : "&limit=10";
        let asc = (filter.asc) ? "&asc=" + filter.asc : "";
        let desc = (filter.desc) ? "&desc=" + filter.desc : "";
        newFilter += page + limit + asc + desc;
        return newFilter;
    }

    

    public async showAlert(title: string, msg: string, subTitle: string, buttons: Array<any>, inputs: Array<any>): Promise<HTMLIonAlertElement> {
        let options: any = {"backdropDismiss": false};
        if(title){options["header"] = title}
        if(msg){options["message"] = msg}
        if(subTitle){options["subHeader"] = subTitle;}
        if(buttons && buttons.length > 0){options["buttons"] = buttons;}
        if(inputs && inputs.length > 0){options["inputs"] = inputs;}
        let alert:HTMLIonAlertElement = await this.alertController.create(options);
        await alert.present();
        return alert;
    }

    public async alert(title: string, msg: string): Promise<HTMLIonAlertElement>{
        let alert: HTMLIonAlertElement = null;
                let buttons = [{text: 'OK', cssClass: 'primary', "handler": () => {
                    if(alert){
                        alert.dismiss();
                    }
                }}];
        await this.showAlert(title, msg, null, buttons, null);
        return alert;
    }

    public async confirm(title: string, msg: string, onConfirm: any, onCancel: any, textConfirm: string, textCancel: string): Promise<HTMLIonAlertElement>{
        let btnConfirm = {};
        let btnCancel = {};
        btnConfirm["text"] = textConfirm ? textConfirm : this.translate.getValue("OK");
        if(onConfirm){btnConfirm["handler"] = onConfirm}
        btnCancel["text"] = textCancel ? textCancel : this.translate.getValue("CANCEL");
        btnCancel["roler"] = "cancel";
        btnCancel["cssClass"] = "secondary";
        if(onCancel){btnCancel["handler"] = onCancel}
        let buttons = [btnCancel, btnConfirm];
        return await this.showAlert(title, msg, null, buttons, null);
    }

    async showToast(msg: string, position: string, duration: number, closeButton: boolean, textButtonClose: string): Promise<HTMLIonToastElement> {
        let options = {};
        options["message"] = msg;
        options["position"] = position ? position : "bottom";
        if(closeButton){options["showCloseButton"] = true}
        if(textButtonClose){options["closeButtonText"] = textButtonClose}
        options["duration"] = duration ? duration : 4000;
        let toast: HTMLIonToastElement = await this.toastController.create(options);
        await toast.present();
        return toast;
    }

    async toask(msg: string, position?: string){
        return await this.showToast(msg, position, null, null, null);
    }

    async showLoading(msg?: string, duration?: number, spinner?: string) {
        let options = {};
        options["message"] = msg ? msg : this.translate.getValue("WAIT_MODAL");
        options["duration"] = duration ? duration : 0;
        if(spinner){options["spinner"] = spinner}
        this.loadingNow = await this.loadingController.create(options);
        await this.loadingNow.present();
    }

    async showModal(component: any, objTranfe: any): Promise<HTMLIonModalElement> {
        const modal = await this.modalController.create({
          component: component,
          componentProps: objTranfe,
          backdropDismiss: false,
        });
        await modal.present()
        return modal;
      }

    async loading(msg: string){
        await this.showLoading(msg, null, null);
    }

    async dismissLoading(){
        this.loadingController.dismiss();
        // if(this.loadingNow){
        //     await this.loadingNow.dismiss();
        // }
    }

    async showMesseges(data: any): Promise<string>{
        let msgErros = "";
        if(data){
            let messages = data[ObjectMSG.MESSAGES];
            // if(data instanceof HttpErrorResponse){
            //     console.log(`Error lista`);
            //     console.log(data);
            // }else{
            //     messages = data[objectMsg.LIST_MSG];
            //     console.log(`lista`);
            //     console.log(data);
            // }
            console.log(messages);
            if(messages){
                messages.forEach(async message => {
                    if(message.type && message.type == TypeMSG.DANGER){
                        if(message.msg){
                            msgErros += this.translate.getValue(message.msg) + "\n";
                        }
                    }else if(message.msg){
                       let toask = await this.toask( this.translate.getValue(message.msg), "bottom");
                       let dismiss = await toask.onDidDismiss();
                    }
                });
            }else if(data instanceof HttpErrorResponse){
                let status = data[ObjectMSG.STATUS];
                if(status >= 0){
                    console.log(`Status: ${status}`);
                    msgErros += this.translate.getValue("status" + status)
                }
            }
            await this.dismissLoading();
            if(msgErros != null && msgErros.length > 0){
                await this.alert(null, msgErros);
                console.log('Confirm Cancel');
            }
        }
        return msgErros;
    }
    
    createFiltersList(list: any): string{
        let filters = "";
        for(var key in list){
            if(!Validation.isOnlyNumbers(list[key])){
                filters +=  key  + "="+  list[key] + "&";
            }else{
                filters +=  key + "=%" + list[key] + "%&";
            }
        }
        return filters;
    }

    concatObjects(sourceObject: any, destinyObject: any): any{
        for(let key in sourceObject){
            destinyObject[key] = sourceObject[key];
        }
        return destinyObject;
    }

    public isPlatformMobile(): boolean {
        if (navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ) {
            return true;
        }
        else {
            return false;
        }
    }

    public async setStorage(key: string, value: any){
        await this.storage.set(key, value);
    }

    public async getStorage(key: string): Promise<any>{
        return await this.storage.get(key);
    }

    public async removeStorage(key: string): Promise<any>{
        return await this.storage.remove(key);
    }
}