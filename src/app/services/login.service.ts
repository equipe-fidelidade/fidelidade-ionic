import { Injectable } from '@angular/core';
import { User } from '../objects/user';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  public login(user: User){
    return this.http.post<User>(`${environment.API}/v1/authenticate`, user).pipe(take(1));
  }
  
  public async logout(){

  }

  public getAuth(){
  }
}
