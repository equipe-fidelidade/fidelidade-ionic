import { HttpClient } from '@angular/common/http';
import { take } from "rxjs/operators";
import { environment } from 'src/environments/environment';

export class CRUDService<T> {
    constructor(protected http: HttpClient, protected path: string){}

    one(id: any){
        return this.http.get<T[]>(`${environment.API}${this.path}/${id}`).pipe(take(1));
    }
    
    all(){
        return this.http.get<T>(`${environment.API}${this.path}`).pipe(take(1));
    }
    
    remove(id: any){
        return this.http.delete(`${environment.API}${this.path}/${id}`).pipe(take(1));
    }

    save(record: T){
        return this.http.post<T>(`${environment.API}${this.path}`, record).pipe(take(1));
    }

    update(record: T){
        return this.http.put<T>(`${environment.API}${this.path}/${record["id"]}`, record).pipe(take(1));
    }
}